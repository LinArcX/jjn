<h4 align="center">
  <img src="https://img.shields.io/github/languages/top/LinArcX/jjn.svg"/>  <img src="https://img.shields.io/github/repo-size/LinArcX/jjn.svg"/>  <img src="https://img.shields.io/github/tag/LinArcX/jjn.svg?colorB=green"/>
</h4>

## Installation

### Install it from source
You can install jjn by compiling from source, here's the list of dependencies required:

#### hostmake dependencies:
 - `pkg-config`
 - `cmake`
 - `gcc` or `clang`

#### buildtime dependencies:
 - `webkitgtk`
 - `gtk3`
 - `libressl`

#### Building
```
git clone https://github.com/LinArcX/jjn
mkdir build; mkdir release; cd build
cmake -DCMAKE_BUILD_TYPE=RELEASE ../;
make
```
And finally, run it:

`./release/jjn`

## What's Jaam-e-jahan-nama?
https://en.wikipedia.org/wiki/Cup_of_Jamshid

## Donate
- Bitcoin: `13T28Yd37qPtuxwTFPXeG9dWPahwDzWHjy`
<img src="assets/donate/Bitcoin.png" width="200" align="center" />

- Monero: `48VdRG9BNePEcrUr6Vx6sJeVz6EefGq5e2F5S9YV2eJtd5uAwjJ7Afn6YeVTWsw6XGS6mXueLywEea3fBPztUbre2Lhia7e`
<img src="assets/donate/Monero.png" align="center" />

## License
![License](https://img.shields.io/github/license/LinArcX/jjn.svg)
