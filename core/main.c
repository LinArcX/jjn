#include <gtk/gtk.h>
#include <stdlib.h>
#include <webkit2/webkit2.h>

#include <girara/datastructures.h>
#include <girara/session.h>
#include <girara/settings.h>
#include <girara/statusbar.h>
#include <girara/utils.h>

static void destroyWindowCb(GtkWidget* widget, GtkWidget* window)
{
    gtk_main_quit();
}

static gboolean closeWebViewCb(WebKitWebView* webView, GtkWidget* window)
{
    gtk_widget_destroy(window);
    return TRUE;
}

int main(int argc, char* argv[])
{
    // Initialize GTK+
    gtk_init(&argc, &argv);

    /*create girara session */
    girara_session_t* session = girara_session_create();

    if (session == NULL) {
        return -1;
    }

    if (girara_session_init(session, NULL) == false) {
        girara_session_destroy(session);
        return -1;
    }

    // Create an 800x600 window that will contain the browser instance
    GtkWidget* main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(main_window), 800, 600);

    // Create a browser instance
    WebKitWebView* webView = WEBKIT_WEB_VIEW(webkit_web_view_new());

    // Put the browser area into the main window
    gtk_container_add(GTK_CONTAINER(main_window), GTK_WIDGET(webView));

    girara_set_view(session, main_window);

    /* add a statusbar entry */
    girara_statusbar_item_t* item = girara_statusbar_item_add(session, TRUE, TRUE, TRUE, NULL);

    if (item != NULL) {
        girara_statusbar_item_set_text(session, item, "girara-left");
    }

    /* read settings */
    int tmp_val_int = 0;
    if (girara_setting_get(session, "window-width", &tmp_val_int) == true) {
        fprintf(stderr, "Window width: %d\n", tmp_val_int);
    } else {
        fprintf(stderr, "Window width: (not set)\n");
    }

    // Set up callbacks so that if either the main window or the browser instance is
    // closed, the program will exit
    g_signal_connect(main_window, "destroy", G_CALLBACK(destroyWindowCb), NULL);
    g_signal_connect(webView, "close", G_CALLBACK(closeWebViewCb), main_window);

    // Load a web page into the browser instance
    webkit_web_view_load_uri(webView, "https://www.google.com/");

    // Make sure that when the browser area becomes visible, it will get mouse
    // and keyboard events
    gtk_widget_grab_focus(GTK_WIDGET(webView));

    // Make sure the main window and all its contents are visible
    gtk_widget_show_all(main_window);

    /* start gtk main loop */
    gtk_main();

    /* clean up */
    girara_session_destroy(session);
    return 0;
}
